<?php

require 'Client.php';

$api_key = "2ac6da30-c59d-11e9-a02b-6dd6eaa423fa";
$secret_key = "MySecret";
$base_url = "http://localhost";

// 1. Create instance of Client with credentials
$client = new Client($base_url, $secret_key, $api_key);

// 2. Get marketdata
$market = $client->getMarketData(
    'BTC', 'USD'
);

print_r($market);

// 2. Place a LIMIT order
$client->placeOrder(
    100000000, // 1.00000000 BTC
    50000, // 5.0000 USD
    $market[0]['id'],
    'LIMIT',
    'BID'
);