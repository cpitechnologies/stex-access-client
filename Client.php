<?php

/**
 * Class Client
 */
class Client {
    protected $base_url;
    protected $secret;
    protected $access_key;

    const ACCESS_SUFFIX = '/access/';
    const CREATE_ORDER_OPERATION = 'order.create';
    const GET_MARKET_DATA = 'get.market.data';

    /**
     * NiceClient constructor.
     * @param $base_url
     * @param $secret
     * @param $access_key
     */
    public function __construct($base_url, $secret, $access_key)
    {
        $this->base_url = $base_url;
        $this->secret = $secret;
        $this->access_key = $access_key;
    }

    /**
     * @param $amount
     * @param $price
     * @param $marketId
     * @param $orderType (MARKET|LIMIT)
     * @param $orderRequestType (BID|ASK)
     * @return string (Order ID)
     * @throws Exception
     */
    public function placeOrder($amount, $price, $marketId, $orderType, $orderRequestType) {

        return $this->request(
            self::CREATE_ORDER_OPERATION,
            [
                'amount'   => $amount,
                'price'    => $price,
                'marketId' => $marketId,
                'orderType' => $orderType,
                'orderRequestType' => $orderRequestType,
            ]
        );
    }

    /**
     * @param $amount
     * @param $price
     * @param $marketId
     * @param $orderType (MARKET|LIMIT)
     * @param $orderRequestType (BID|ASK)
     * @return string (Order ID)
     * @throws Exception
     */
    public function getMarketData($fromAsset = null, $toAsset = null) {

        return $this->request(
            self::GET_MARKET_DATA,
            [
                'fromAsset'   => $fromAsset,
                'toAsset'   => $toAsset,
            ]
        );
    }

    /**
     * @param $operation
     * @param $payload
     * @return mixed
     * @throws Exception
     */
    protected function request($operation, $payload) {
        $url = $this->base_url . self::ACCESS_SUFFIX . $operation;

        $request = [
            'api_key' => $this->access_key,
            'signature' => $this->sign($payload),
            'body' => json_encode($payload)
        ];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

        $response = curl_exec($ch);
        
        $code =  curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $result = json_decode($response, true);

        curl_close($ch);

        if($code === 200) {
            return $result['data'];
        } else if($code === 403) {
            throw new \Exception('Wrong credentials or user is locked! Don\'t repeat this too often or your IP adress will be blocked.', 403);
        } else if($code === 422) {
            throw new \Exception('Validation error: ' . print_r($result['errors'], true), 422);
        } else if($code === 404) {
            throw new \Exception('Your API key does not allow this operation or it was removed.', 404);
        } else {
            throw new \Exception('Unexpected error!', 500);
        }
    }

    /**
     * @param $payload
     * @return string
     */
    protected function sign($payload) {
        return sha1(
            $this->access_key. '-'
            . $this->secret . '-'
            . json_encode($payload)
        );
    }
}